package ml;

import java.util.Arrays;
import java.util.List;
import org.apache.spark.ml.feature.Word2Vec;
import org.apache.spark.ml.feature.Word2VecModel;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.*;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.feature.HashingTF;
import org.apache.spark.ml.feature.Tokenizer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class imdb {
    public static void main() 
	{
// Инициализация сессии
SparkSession spark = SparkSession.builder()
           .appName("IMDB Sentiment Analysis")
           .master(master)
           .getOrCreate();

// Загружаем тестовые данные
       Dataset<Row> training = spark.read().format("csv")
       .option("quote", "\"")
       .option("escape","\"")
       .load("/user/hduser/Train.csv")
       .withColumn("label", col("label").cast(DataTypes.FloatType));
// определяем токенайзер
       Tokenizer tokenizer = new Tokenizer().setInputCol("text").setOutputCol("words");
// удаляем стоп слова
       StopWordsRemover remover = new StopWordsRemover()
        .loadDefaultStopWords("english")
        .setInputCol("words")
        .setOutputCol("words2");
// преобразование из слов в вектора
       Word2Vec word2Vec = new Word2Vec()
        .setInputCol("words2")
        .setOutputCol("features")
        .setVectorSize(10)
        .setMinCount(0);
// логистическая регрессия
        LogisticRegression lr = new LogisticRegression()
        .setFeaturesCol("features")
        .setLabelCol("label")
        .setMaxIter(10)
        .setRegParam(0.001);
// определение пайплайна
       Pipeline pipeline = new Pipeline()
        .setStages(new PipelineStage[] {tokenizer,remover,word2Vec,lr});
      
       PipelineModel model = pipeline.fit(training);

// тестовый документ для проверки модели
Dataset<Row> test = spark.read().format("csv")
                         .load("/user/hduser/Valid.csv");

Dataset<Row> predictions = model.transform(test);
for (Row r : test.select("id", "text", "probability", "prediction").collectAsList()) {
       System.out.println("(" + r.get(0) + ", " + r.get(1) + ") --> prob=" + r.get(2)
         + ", prediction=" + r.get(3));
     }
   }
}
